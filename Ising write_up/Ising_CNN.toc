\contentsline {section}{\numberline {1}{Introduction}}{2}{section.1}
\contentsline {section}{\numberline {2}{Basic Structure of a CNN}}{2}{section.2}
\contentsline {subsubsection}{\numberline {2.0.1}{Convolution layers}}{3}{subsubsection.2.0.1}
\contentsline {section}{\numberline {3}{Ising Model}}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}{Sampling and Average}}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}{Calculation of Observables}}{6}{subsection.3.2}
\contentsline {section}{\numberline {4}{Sampling in Monto-Carlo Metropolis method for 2D Ising model}}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}{Metropolis Method}}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}{Sampling of data}}{7}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}{Calculation of observables for sampled data}}{9}{subsection.4.3}
\contentsline {section}{\numberline {5}{model description}}{10}{section.5}
\contentsline {section}{\numberline {6}{Glossary}}{10}{section.6}
