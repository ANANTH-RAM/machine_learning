
# coding: utf-8

# In[1]:


from numpy import *
N0=2 #input layer
N1=1 #output layer


# In[2]:


W=random.uniform(low=-1,high=+1,size=(N1,N0)) #randomly assign weights for input to output layer
B=random.uniform(low=-1,high=+1,size=N1) #randomly assign biases for output


# In[3]:


def apply_NET(Y_in):
    global W,B
    z=dot(W,Y_in)+B # matrix and a vector multiplication added to biases
    return(1/(1+exp(-z))) # calculated the output ; sigmoid function 


# In[4]:


apply_NET([0.9,0.2])


# In[5]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[6]:


M=50 
Y_out=zeros([M,M]) # create picture of 50X50 and write the results in it
for j1 in range(M):
    for j2 in range(M):
        value0=float(j1)/M-0.5
        value1=float(j2)/M-0.5
        Y_out[j1,j2]=apply_NET([value0,value1])[0]


# In[7]:


plt.imshow(Y_out,origin='lower',extent=(-0.5,0.5,-0.5,0.5))
plt.colorbar()
plt.show()

